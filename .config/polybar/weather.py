#!/usr/bin/env python

import json
import urllib
import urllib.parse
import urllib.request
import os


def main():
    id = "4265737"
    api_key = "430241fba47b14c7e96e7386b9a7f2b3"

    try:
        data = urllib.parse.urlencode({'id': id, 'appid': api_key, 'units': 'imperial'})
        weather = json.loads(urllib.request.urlopen(
            'http://api.openweathermap.org/data/2.5/weather?' + data)
            .read())
        desc = weather['weather'][0]['description'].capitalize()
        temp = int(float(weather['main']['temp']))
        #return '{}, {}°F'.format(desc, temp)
        return '{}°F'.format(temp)
    except:
        return ''


if __name__ == "__main__":
	print(main())
