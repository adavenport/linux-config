#!/usr/bin/env python3
# -*- coding: utf8 -*-
"""
Modified script from below
bat

========================================
oooo    oooo               .            
`888   .8P'              .o8            
 888  d8'     .ooooo.  .o888oo  .oooo.  
 88888[      d88' `88b   888   `P  )88b 
 888`88b.    888   888   888    .oP"888 
 888  `88b.  888   888   888 . d8(  888 
o888o  o888o `Y8bod8P'   "888" `Y888""8o
               @nilsu.org               
=== Copyright (C) 2017  Dakota Walsh ===
"""

# imports
import os
import sys
import argparse

# constants
AC   = '/sys/class/power_supply/AC'
BAT0 = '/sys/class/power_supply/BAT0'
BAT1 = '/sys/class/power_supply/BAT1'
bat0 = ''
bat1 = ''
bat0Max = ''
bat1Max = ''

def getArgs():
    # get the arguments with argparse
    description = "Battery Status"
    arg = argparse.ArgumentParser(description=description)

    arg.add_argument("-r", action="store_true",
            help="Print battery percentage instead.")
    return arg.parse_args()

def main():
    arguments = getArgs()
    if arguments.r:
        raw = True
    else:
        raw = False

    f = open(os.path.join(AC, 'online'), 'r')
    ac = f.readline()
    f.close()

    try:
        f = open(os.path.join(BAT0, 'energy_full'), 'r')
        bat0Max = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT0, 'energy_now'), 'r')
        bat0 = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT0, 'charge_full'), 'r')
        bat0Max = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT0, 'charge_now'), 'r')
        bat0 = f.readline()
        f.close()
    except:
        pass

    try:
        # get battery charge amounts
        f = open(os.path.join(BAT1, 'energy_full'), 'r')
        bat1Max = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT1, 'energy_now'), 'r')
        bat1 = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT1, 'charge_full'), 'r')
        bat1Max = f.readline()
        f.close()
    except:
        pass

    try:
        f = open(os.path.join(BAT1, 'charge_now'), 'r')
        bat1 = f.readline()
        f.close()
    except:
        pass

    # check that battery values were found
    if (bat0 == ''):
        sys.exit("battery 0 not found")
    if (bat1 == ''):
        sys.exit("battery 1 not found")
    if (bat0Max == ''):
        sys.exit("battery 0 not found")
    if (bat1Max == ''):
        sys.exit("battery 1 not found")

    # convert ac to boolean
    if (int(ac) == 0):
        ac = False
    else:
        ac = True

    # convert to percent
    bat0Percent = (int(bat0) / int(bat0Max)) * 100
    bat1Percent = (int(bat1) / int(bat1Max)) * 100

    batPercent = (bat0Percent + bat1Percent) / 2

    padding = " "

    ramp_capacity_0 = ' '
    ramp_capacity_1 = ' '
    ramp_capacity_2 = ' '
    ramp_capacity_3 = ' '
    ramp_capacity_4 = ' '

    batPercent = int(batPercent)
    message = ''

    # printing prefix
    if (ac == True):
        message = " " + str(batPercent) # charging symbol
    else:
        if (batPercent <= 15):
            message = ramp_capacity_0 + str(batPercent) # empty battery symbol
        elif (batPercent > 15) and int(batPercent <= 25):
            message = ramp_capacity_1 + str(batPercent) # empty battery symbol
        elif (batPercent > 25 and batPercent <= 50):
            message = ramp_capacity_2 + str(batPercent) # low battery symbol
        elif (batPercent > 50 and batPercent <= 75):
            message = ramp_capacity_3 + str(batPercent) # low battery symbol
        elif (batPercent > 75):
            message = ramp_capacity_4 + str(batPercent) # full battery symbol

    message += '%  '

    if (raw):
        print(batPercent)
    else:
        print(message + padding)

if __name__ == '__main__':
    try:
        main()
    except:
        pass
