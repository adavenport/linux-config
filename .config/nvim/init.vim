" Setting up plugins and color schemes
call plug#begin('~/.vim/plugged')

    Plug 'https://github.com/xolox/vim-misc'
    Plug 'https://github.com/xolox/vim-notes'
    Plug 'junegunn/goyo.vim'
    Plug 'https://github.com/ying17zi/vim-live-latex-preview'
    Plug 'lervag/vimtex'

call plug#end()

" Set the current color scheme
" colorscheme VisualStudioDark

" Bindings
vnoremap <C-c> "*Y :let @+=@*<CR>
map <C-v> "+P

" Setting up hybrid numbers
set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

set ruler

set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.

set incsearch       " While typing a search command, show immediately where the
                    " so far typed pattern matches.

set autoindent      " Copy indent from current line when starting a new line
set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

set nocompatible

set wildmode=longest,list,full
set wildmenu

" Latex
autocmd BufRead,BufNewFile *.tex set filetype=tex

" Goyo
map <F10> :Goyo<CR>
map <leader>f :Goyo \| set linebreak<CR>
inoremap <F10> <esc>:Goyo<CR>a



" auto detect filetype
filetype plugin on

" Map Ctrl-Backspace to delete the previous word in insert mode.
imap <C-BS> <C-W>
set backspace=indent,eol,start
noremap! <C-BS> <C-w>
noremap! <C-h> <C-w>

